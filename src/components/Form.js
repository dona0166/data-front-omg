import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { isEmpty } from "../utilities/validation";

const REQUIRED = ["email", "password"];

export default function Form() {
  const [values, setValues] = useState({
    email: "",
    password: "",
    rememberMe: false,
  });
  const [errors, setErrors] = useState([]);
  const [started, setStarted] = useState(false);

  const handleErrors = useCallback(() => {
    for (const key in values) {
      if (REQUIRED.includes(key)) {
        /* check errors */
        if (isEmpty(values[key])) {
          if (!errors.includes(key)) {
            setErrors((prevState) => [...prevState, key]);
          }
        } else {
          if (errors.includes(key)) {
            setErrors((errors) => errors.filter((error) => error !== key));
          }
        }
      }
    }
  }, [values, errors]);

  useEffect(() => {
    handleErrors();
  }, [handleErrors, values]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      /* move to state management and replace with base url from .env */
      await axios.post("localhost:8000/api/login", values);
    } catch (err) {
      console.log("Don't worry, everything is ok...");
    }
  };

  const handleChange = (e) => {
    setStarted(true);

    const { name, value, type, checked } = e.target;

    if (type === "checkbox") {
      setValues((prevState) => ({
        ...prevState,
        [name]: checked,
      }));
    } else {
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
  };

  return (
    <div className="form form--50">
      <form className="form__group" action="">
        <div className="form__title">Sign in</div>
        <div className="form__input vertical m-y-10">
          <label htmlFor="email" className="form__label bold">
            Email
          </label>
          <input
            id="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            type="text"
            className="form__field"
          />
          <div
            className={`form__error ${
              errors.includes("email") && started && "form__error--active"
            }`}
          >
            Email is required
          </div>
        </div>
        <div className="form__input vertical m-y-10">
          <label htmlFor="password" className="form__label bold">
            Password
          </label>
          <input
            id="password"
            name="password"
            value={values.password}
            onChange={handleChange}
            type="password"
            className="form__field"
          />
          <div
            className={`form__error ${
              errors.includes("password") && started && "form__error--active"
            }`}
          >
            Password is required
          </div>
        </div>
        <div className="form__input center m-y-10">
          <input
            id="rememberMe"
            name="rememberMe"
            value={values.rememberMe}
            onChange={handleChange}
            type="checkbox"
            className="form__field"
          />
          <label htmlFor="rememberMe" className="form__label m-x-5">
            Remember me
          </label>
        </div>
        <div className="form__input">
          <input
            disabled={errors.length > 0}
            onClick={handleSubmit}
            type="submit"
            className="button"
            value="Sign in"
          />
        </div>
        <div className="form__link block m-y-10">
          <a className="link" href="/forgot-password">
            Forgot your password
          </a>
        </div>
        <div className="form__link block m-y-10">
          Don't have an account{" "}
          <a className="link" href="/sign-up">
            Sign up
          </a>
        </div>
        <div
          className="form__link"
          onClick={() => console.log("Sending email")}
        >
          <span className="link">Resend email confirmation</span>
        </div>
      </form>
    </div>
  );
}
